""" Platzigram views """

# Django
from django.http import HttpResponse

# Utils
from datetime import datetime
import json


def hello_world(request):
    """ Return greetings """
    #now = datetime.now().strftime('%b %dth, %Y - %H:%M hrs')
    return HttpResponse('Oh, hi! current server time is {now}'.format(
        now = datetime.now().strftime('%b %dth, %Y - %H:%M hrs')
    ))


def sort_integers(request):
    """ Return a JSON response with sorted integers. """
    print(request)
    # for does debug
    #import pdb; pdb.set_trace()
    numbers = [int(i) for i in request.GET['numbers'].split(',')]
    sorted_numbers = sorted(numbers)
    data = {
        'status': 'ok',
        'numbers': sorted_numbers,
        'message': 'Integers sorted successfully'
    }
    return HttpResponse(
        json.dumps(data, indent=4),
        content_type='application/json'
    )

def say_hi(request, name, age):
    """ Greetings for name """
    if age < 12:
        message = 'Sorry {}, you are not allowed here'.format(name)
    else:
        message = 'Hello {}, welcome to platzigram '.format(name)
    
    return HttpResponse(message)
    #return HttpResponse('Hello {}, you are {} years old!'.format(name, age))